// BumpInt.c
// Runs on MSP432, interrupt version
// Provide low-level functions that interface bump switches the robot.
// Daniel Valvano and Jonathan Valvano
// July 2, 2017

/* This example accompanies the books
   "Embedded Systems: Introduction to the MSP432 Microcontroller",
       ISBN: 978-1512185676, Jonathan Valvano, copyright (c) 2017
   "Embedded Systems: Real-Time Interfacing to the MSP432 Microcontroller",
       ISBN: 978-1514676585, Jonathan Valvano, copyright (c) 2017
   "Embedded Systems: Real-Time Operating Systems for ARM Cortex-M Microcontrollers",
       ISBN: 978-1466468863, , Jonathan Valvano, copyright (c) 2017
 For more information about my classes, my research, and my books, see
 http://users.ece.utexas.edu/~valvano/

Simplified BSD License (FreeBSD License)
Copyright (c) 2017, Jonathan Valvano, All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are
those of the authors and should not be interpreted as representing official
policies, either expressed or implied, of the FreeBSD Project.
*/

// Negative logic bump sensors
// P4.7 Bump5, left side of robot
// P4.6 Bump4
// P4.5 Bump3
// P4.3 Bump2
// P4.2 Bump1
// P4.0 Bump0, right side of robot

#include <stdint.h>
#include "msp.h"
void (*BumpTask)(uint8_t);
// Initialize Bump sensors
// Make six Port 4 pins inputs
// Activate interface pullup
// pins 7,6,5,3,2,0
// Interrupt on falling edge (on touch)
void BumpInt_Init(void(*task)(uint8_t)){
    // write this as part of Lab 14
	BumpTask = task;
	
	
	// Enable pull resistors
	 //initializing Bump sensors port 4 pins 7,6,5,3,2,0
	    //making them inputs
	    P4->SEL0 &= ~(0xED);
	    P4->SEL1 &= ~(0xED);
	    P4->DIR &= ~(0xED);

	    //Active interface pullup
	    P4->REN |= 0xED;
	    P4->OUT |= (0xED); //negative logic
	
	
	// Set to falling edge events PxIFG flag is set with a high-to-low transition
	    P4->IES |= 0xED;
	// Clear all flags (IFG -> no interrupts pending)
	    P4->IFG &= ~0xED;
	// Arm interrupt (enable IE) on P4.7-P4.5, P4.3-P4.2, P4.0
	  P4->IE |= 0xED;
	// Setup the Nested Vector Interrupt Controller; priority 0
	  // make sure you choose the correct NVIC
	  NVIC -> IP[9]= (NVIC -> IP[9] & 0xFF00FFFF)| 0x00000000;

	// Enable appropriate interrupt
	  NVIC->ISER[1] = 0x00000040; //interrupt 38
	
}
// Read current state of 6 switches
// Returns a 6-bit positive logic result (0 to 63)
// bit 5 Bump5
// bit 4 Bump4
// bit 3 Bump3
// bit 2 Bump2
// bit 1 Bump1
// bit 0 Bump0
uint8_t Bump_Read(void){
    // write this as part of Lab 14 (should be very similar to lab 10
    uint8_t bump5 = 0;
    uint8_t bump4 =0;
    uint8_t bump3 =0;
    uint8_t bump2 =0;
    uint8_t bump1 =0;
    uint8_t bump0 =0;
    uint8_t data =0;
    uint8_t bump = 0;
    data = P4->IN;

    bump5 = (data & (0x80)) >> 2;
    bump4 = (data & (0x40)) >> 2;
    bump3 = (data & (0x20)) >> 2;
    bump2 = (data & (0x08)) >> 1;
    bump1 = (data & (0x04)) >> 1;
    bump0 = (data & (0x01));
    bump = bump5 | bump4 | bump3 | bump2 | bump1 | bump0;
    return bump;

}
// we do not care about critical section/race conditions
// triggered on touch, falling edge
void PORT4_IRQHandler(void){
    // write this as part of Lab 14
	
	//ack/clear interrupt flag (No interrupt is pending)
	P4->IFG &= ~0xED;
	// Call Bump_Read()
	(*BumpTask)(Bump_Read());
}

