// Lab17_Control.c
// Runs on MSP432
// Implementation of the control system.
// Daniel and Jonathan Valvano
// September 12, 2017

/* This example accompanies the books
   "Embedded Systems: Introduction to the MSP432 Microcontroller",
       ISBN: 978-1512185676, Jonathan Valvano, copyright (c) 2017
   "Embedded Systems: Real-Time Interfacing to the MSP432 Microcontroller",
       ISBN: 978-1514676585, Jonathan Valvano, copyright (c) 2017
   "Embedded Systems: Real-Time Operating Systems for ARM Cortex-M Microcontrollers",
       ISBN: 978-1466468863, , Jonathan Valvano, copyright (c) 2017
 For more information about my classes, my research, and my books, see
 http://users.ece.utexas.edu/~valvano/

Simplified BSD License (FreeBSD License)
Copyright (c) 2017, Jonathan Valvano, All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are
those of the authors and should not be interpreted as representing official
policies, either expressed or implied, of the FreeBSD Project.
*/

#include <stdint.h>
#include "msp.h"
#include "../inc/Clock.h"
#include "../inc/CortexM.h"
#include "../inc/PWM.h"
#include "../inc/LaunchPad.h"
#include "../inc/UART0.h"
#include "../inc/Motor.h"
#include "../inc/Bump.h"
#include "../inc/ADC14.h"
#include "../inc/TimerA1.h"
#include "../inc/IRDistance.h"
#include "../inc/Nokia5110.h"
#include "../inc/LPF.h"
#include "../inc/FlashProgram.h"
#include "../inc/SysTickInts.h"
#include "../inc/Tachometer.h"
#include "../inc/Reflectance.h"
#include "../inc/Classifier.h"

/**************Initial values used for all programs******************/

volatile uint32_t ControllerFlag; // set every 10ms on controller execution



int32_t Mode = 0;
int32_t UL, UR;             // Controller output PWM duty 2 to 14,998
uint32_t Time; // in 0.01 sec
//
//// proportional controller gain
//// experimentally determine value that creates a stable system
//// may want a different gain for each program
//int32_t Kp1= 93;
//
///**************Functions used by all programs***********************/

//
///**************Program17_1******************************************/
//#define DESIRED_SPEED 100
//#define TACHBUFF 10                      // number of elements in tachometer array
//
//int32_t ActualSpeedL, ActualSpeedR;   	 // Actual speed
//int32_t ErrorL, ErrorR;     			 // X* - X'
//uint16_t LeftTach[TACHBUFF];             // tachometer period of left wheel (number of 0.0833 usec cycles to rotate 1/360 of a wheel rotation)
//enum TachDirection LeftDir;              // direction of left rotation (FORWARD, STOPPED, REVERSE)
//int32_t LeftSteps;                       // number of tachometer steps of left wheel (units of 220/360 = 0.61 mm traveled)
//uint16_t RightTach[TACHBUFF];            // tachometer period of right wheel (number of 0.0833 usec cycles to rotate 1/360 of a wheel rotation)
//enum TachDirection RightDir;             // direction of right rotation (FORWARD, STOPPED, REVERSE)
//int32_t RightSteps;                      // number of tachometer steps of right wheel (units of 220/360 = 0.61 mm traveled)
//
//int i = 0;
//
//void LCDClear1(void){
//    Nokia5110_Init();
//    Nokia5110_Clear();
//    Nokia5110_OutString("Desired(RPM)L     R     Actual (RPM)L     R     Error(RPM)  L     R     ");
//}
//void LCDOut1(void){
//    Nokia5110_SetCursor(1, 1);         // one leading space, second row
//    Nokia5110_OutSDec1(DESIRED_SPEED);
//    Nokia5110_SetCursor(7, 1);         // seven leading spaces, second row
//    Nokia5110_OutSDec1(DESIRED_SPEED);
//    Nokia5110_SetCursor(1, 3);       // one leading space, fourth row
//    Nokia5110_OutUDec(ActualSpeedL);
//    Nokia5110_SetCursor(7, 3);       // seven leading spaces, fourth row
//    Nokia5110_OutUDec(ActualSpeedR);
//    Nokia5110_SetCursor(1, 5);       // zero leading spaces, sixth row
//    Nokia5110_OutSDec1(ErrorL);
//    Nokia5110_SetCursor(7, 5);       // six leading spaces, sixth row
//    Nokia5110_OutSDec1(ErrorR);
//}
//
//// ------------avg------------
//// Simple math function that returns the average
//// value of an array.
//// Input: array is an array of 16-bit unsigned numbers
////        length is the number of elements in 'array'
//// Output: the average value of the array
//// Note: overflow is not considered
uint16_t avg(uint16_t *array, int length){
  int i;
  uint32_t sum = 0;
  for(i=0; i<length; i=i+1){
    sum = sum + array[i];
  }
  return (sum/length);
}
//
////**************************************************
//// Proportional controller to drive straight with no
//// sensor input
//void Controller1(void){
//
//    if(Mode){
//		// pull tachometer information
//        Tachometer_Get(&LeftTach[i], &LeftDir, &LeftSteps, &RightTach[i], &RightDir, &RightSteps);
//
//		// use average of ten tachometer values to determine
//		// actual speed (similar to Lab16)
//        i = i + 1;
//        if(i >= TACHBUFF){
//          i = 0;
//          ActualSpeedL = 2000000/avg(LeftTach, TACHBUFF);
//          ActualSpeedR = 2000000/avg(RightTach, TACHBUFF);
//          ErrorL = DESIRED_SPEED - ActualSpeedL;
//          ErrorR = DESIRED_SPEED - ActualSpeedR;
//
//			// use proportional control to update duty cycle
//			// LeftDuty = LeftDuty + Kp*LeftError
//			UR = UR + ((Kp1*ErrorR)/100);
//			UL = UL + ((Kp1*ErrorL)/100);
//
//			// check min/max duty values
//			if(2 > UR){
//			    UR = 2;
//			}
//			if(14998 < UR){
//			    UR = 14998;
//			}
//			if(2 > UL){
//			    UL = 2;
//			}
//			if(14998 < UL){
//			    UL = 14998;
//			}
//
//			// update motor values
//            Motor_Forward(UL, UR);
//			ControllerFlag = 1;
//        }
//    }
//}
//
//// go straight with no sensor input
//void Program17_1(void){
//  DisableInterrupts();
//// initialization
//  Clock_Init48MHz();
//  LaunchPad_Init();
//  Bump_Init();
//  Tachometer_Init();
//  Motor_Init();
//
//  // user TimerA1 to run the controller at 100 Hz
//  // replace this line with a call to TimerA1_Init()
//  TimerA1_Init(&Controller1,5000);
//  Motor_Stop();
//  UR = UL = PWMNOMINAL;
//  EnableInterrupts();
//  LCDClear1();
//  ControllerFlag = 0;
//  Pause3();
//
//	while(1){
//		if(Bump_Read() != 0x3f){
//			Mode = 0;
//			Motor_Stop();
//			Pause3();
//		}
//		if(ControllerFlag == 1){
//			LCDOut1();
//			ControllerFlag = 0;
//		}
//  }
//}
///**************Program17_2******************************************/
//// distances in mm
//#define TOOCLOSE 110
//#define DESIRED_DIST 172
//#define TOOFAR 230
//
//volatile uint32_t nr, nc, nl; // raw distance values
//int32_t Left, Center, Right; // IR distances in mm
//volatile uint32_t ADCflag; // Set every 500us on ADC sample
//int32_t DataBuffer[5];
//int32_t SetPoint = 172;
//uint32_t PosError;
//int32_t Error2;
//
//scenario_t wall_state;
//
//
//int Kp2 = 8;
//void LCDClear2(void){
//  Nokia5110_Init();
//  Nokia5110_Clear(); // erase entire display
//  Nokia5110_OutString("17: control");
//  Nokia5110_SetCursor(0,1); Nokia5110_OutString("IR distance");
//  Nokia5110_SetCursor(0,2); Nokia5110_OutString("L= "); Nokia5110_OutSDec(0); Nokia5110_OutString(" mm");
//  Nokia5110_SetCursor(0,3); Nokia5110_OutString("C= "); Nokia5110_OutSDec(0); Nokia5110_OutString(" mm");
//  Nokia5110_SetCursor(0,4); Nokia5110_OutString("R= "); Nokia5110_OutSDec(0); Nokia5110_OutString(" mm");
//  Nokia5110_SetCursor(0,5); Nokia5110_OutString("E= "); Nokia5110_OutSDec(0); Nokia5110_OutString(" mm");
//}
//
//void LCDOut2(void){
//  Nokia5110_SetCursor(3,2); Nokia5110_OutSDec(Left);
//  Nokia5110_SetCursor(3,3); Nokia5110_OutSDec(Center);
//  Nokia5110_SetCursor(3,4); Nokia5110_OutSDec(Right);
//  Nokia5110_SetCursor(3,5); Nokia5110_OutSDec(Error);
//  // left
//  if(Time%5 == 0){
//      UART0_OutUDec5(Left);UART0_OutString(" mm,");
//      UART0_OutUDec5(Center);UART0_OutString(" mm,");
//      UART0_OutUDec5(Right);UART0_OutString(" mm,");
//      UART0_OutUDec5(UR);UART0_OutString(" %,");
//      UART0_OutUDec5(UL);UART0_OutString(" %,");
//      if(Error < 0){
//          PosError = Error*(-1);
//          UART0_OutString("-");UART0_OutUDec5(PosError);UART0_OutString("\n");
//      }
//      else{
//          UART0_OutUDec5(Error);UART0_OutString("\n");
//      }
//
//  }
//}
//
//void IRsampling(void){
//    uint32_t raw17, raw12, raw16;
//    ADC_In17_12_16(&raw17, &raw12, &raw16);
//    nr = LPF_Calc(raw17);
//    nc = LPF_Calc2(raw12);
//    nl = LPF_Calc3(raw16);
//    Left = LeftConvert(nl);
//    Center = CenterConvert(nc);
//    Right = RightConvert(nr);
//    ADCflag = 1;
//}
//
///*
//* Proportional controller to keep robot in
//* center of two walls using IR sensors.
//*/
//void SysTick_Handler(void){
//    if(Mode){
//
//        wall_state = Classify(Left, Center, Right);
//
//        if(wall_state > Straight){
//
//        }
//
//
//        // Determine set point
//        if(Left > DESIRED_DIST && Right > DESIRED_DIST){
//            SetPoint = (Left + Right) / 2;
//        }
//        else{
//            SetPoint = 172;
//        }
//        // set error based off set point
//        if(Left < Right){
//            Error2 = Left - SetPoint;
//        }else{
//            Error2 = SetPoint - Right;
//        }
//
//        // update duty cycle based on proportional control
//        UR = UR + Kp2 * Error2;
//        UL = UL - Kp2 * Error2;
//
//        // check to ensure not too big of a swing
//        if(UR < PWMIN){
//            UR = PWMIN;
//        }
//        if(UR > PWMAX){
//            UR = PWMAX;
//        }
//        if(UL < PWMIN){
//            UL = PWMIN;
//        }
//        if(UL > PWMAX){
//            UL = PWMAX;
//        }
//        // update motor values
//        Motor_Forward(UL, UR);
//        ControllerFlag = 1;
//    }
//}
//
//// proportional control, wall distance
//void Program17_2(void){
//    uint32_t raw17,raw12,raw16;
//    DisableInterrupts();
//    Clock_Init48MHz();
//    LaunchPad_Init();
//    Bump_Init();
//    Motor_Init();
//
//    // user TimerA1 to sample the IR sensors at 2000 Hz
//    // replace this line with a call to TimerA1_Init()
//    TimerA1_Init(&IRsampling, 250);
//    Motor_Stop();
//    LCDClear2();
//    Mode = 0;
//    UR = UL = PWMNOMINAL;
//    ADCflag = ControllerFlag = 0;   // semaphores
//
//    ADC0_InitSWTriggerCh17_12_16();   // initialize channels 17,12,16
//    ADC_In17_12_16(&raw17,&raw12,&raw16);  // sample
//    LPF_Init(raw17,64);     // P9.0/channel 17
//    LPF_Init2(raw12,64);    // P4.1/channel 12
//    LPF_Init3(raw16,64);    // P9.1/channel 16
//
//    // user SysTick to run the controller at 100 Hz with a priority of 2
//    // replace this line with a call to SysTick_Init()
//    SysTick_Init(480000,2);
//    Pause3();
//
//    EnableInterrupts();
//    while(1){
//        if(Bump_Read() != 0x3f){ // collision
//            Mode = 0;
//            Motor_Stop();
//            Pause3();
//        }
//        if(ControllerFlag == 1){ // 100 Hz, not real time
//            LCDOut2();
//            ControllerFlag = 0;
//        }
//    }
//}
///**************Program17_3******************************************/
//
//uint8_t LineData;       // direct measure from line sensor
//int32_t Position;      // position in 0.1mm relative to center of line
//
//void LCDClear3(void){
//  Nokia5110_Init();
//  Nokia5110_Clear(); // erase entire display
//  Nokia5110_OutString("17: control");
//  Nokia5110_SetCursor(0,1); Nokia5110_OutString("Line Follow");
//  Nokia5110_SetCursor(0,2); Nokia5110_OutString("D =  "); Nokia5110_OutUDec(0);
//  Nokia5110_SetCursor(0,3); Nokia5110_OutString("P = "); Nokia5110_OutSDec(0);
//  Nokia5110_SetCursor(0,4); Nokia5110_OutString("UR=  "); Nokia5110_OutUDec(0);
//  Nokia5110_SetCursor(0,5); Nokia5110_OutString("UL=  "); Nokia5110_OutUDec(0);
//}
//void LCDOut3(void){
//  Nokia5110_SetCursor(5,2); Nokia5110_OutUDec(LineData);
//  Nokia5110_SetCursor(4,3); Nokia5110_OutSDec(Position);
//  Nokia5110_SetCursor(5,4); Nokia5110_OutUDec(UR);
//  Nokia5110_SetCursor(5,5); Nokia5110_OutUDec(UL);
//}
//
///*
//* Proportional controller to drive robot
//* using line following
//*/
//int32_t change = 0;
//int Kp3 = 10;
//void Controller3(void){
//
//    // read values from line sensor, similar to
//    // SysTick_Handler() in Lab10_Debugmain.c
//    Time = Time + 1;
//    if(Time == 11){
//        Reflectance_Start();
//    }
//    if(Time == 12){
//        LineData = Reflectance_End();
//        Time = 0;
//        Position = Reflectance_Position(LineData);
//
//            if(Mode){
//                // if robot is too far right of line (Position > 0)
//                // increase change
//                UR = PWMNOMINAL - Kp3 * Position;
//                UL = PWMNOMINAL + Kp3 * Position;
//
//                // limit change to within swing
//                if(UR < PWMIN){
//                    UR = PWMIN;
//                }
//                if(UL < PWMIN){
//                    UL = PWMIN;
//                }
//                if(UR > PWMAX){
//                    UR = PWMAX;
//                }
//                if(UL > PWMAX){
//                    UL = PWMAX;
//                }
//
//                // update duty cycle based on porportional control
//                Motor_Forward(UL, UR);
//                ControllerFlag = 1;
//    }
//    // Use Reflectance_Position() to find position
//
//    }
//}
//
//// proportional control, line following
//void Program17_3(void){
//    DisableInterrupts();
//    Clock_Init48MHz();
//    LaunchPad_Init();
//    Bump_Init();
//    Reflectance_Init();
//    Motor_Init();
//
//    // user TimerA1 to run the controller at 1000 Hz
//    // replace this line with a call to TimerA1_Init()
//    TimerA1_Init(&Controller3, 5000);
//    Motor_Stop();
//    LCDClear3();
//    Mode = 0;
//    Time = 0;
//    UR = UL = PWMNOMINAL;
//    EnableInterrupts();
//    Pause3();
//    while(1){
//      if(Bump_Read()!= 0x3f){ // collision
//        Mode = 0;
//        Motor_Stop();
//        Pause3();
//      }
//      if(ControllerFlag == 1){ // 100 Hz , not real time
//        LCDOut3();
//        ControllerFlag = 0;
//      }
//    }
//}
#define PWMNOMINAL 3000

#define SWING 750
#define PWMIN (PWMNOMINAL-SWING)
#define PWMAX (PWMNOMINAL+SWING)
#define TACHBUFF 10
uint16_t LeftTach[TACHBUFF];            // tachometer period of left wheel (number of 0.0833 usec cycles to rotate 1/360 of a wheel rotation)
enum TachDirection LeftDir;
int32_t LeftSteps;
uint16_t RightTach[TACHBUFF];
enum TachDirection RightDir;
int32_t RightSteps;
uint8_t LineData = 24;       // direct measure from line sensor
int32_t Position;      // position in 0.1mm relative to center of line
int Kp4 = 20;
int Bump = 0;
int leftLast = -1;
int tachStep = -300;
int Hit = 0;
int Exit = 0;
int j =0;
int turn = 0;
void Controller4(void){
    if(Bump_Read()!= 0x3f){ // collision
       Motor_Stop();
      if(Hit == 1){
          Exit = 1;
          Motor_Stop();
     }else{
         Mode = 2;
         Time = 0;
     }
    }


    Time = Time + 1;
    Tachometer_Get(&LeftTach[j], &LeftDir, &LeftSteps, &RightTach[j], &RightDir, &RightSteps);
    if(Mode == 2){
        if(tachStep == -300){
            Motor_Backward(PWMNOMINAL, PWMNOMINAL);
            if(Time >= 300){
                tachStep = 0;

            }
        }
        else{

            if(leftLast == -1){
                leftLast = LeftSteps;
            }
            tachStep = LeftSteps - leftLast;
            turn =  (70 * tachStep) / 150;
            if( turn >= 180){
                Mode = 1;
                Time = 0;

            }else{
            UR = PWMNOMINAL + 2000;
            UL = PWMNOMINAL + 2000;
            Motor_Left(UL, UR);
            Mode = 2;
            }
            Hit = 1;
            //ControllerFlag = 1;

        }
    }
    else{
        if(Time == 11){
            Reflectance_Start();
        }
        if(Time == 12){
            LineData = ~Reflectance_End();
            Time = 0;
            Position = Reflectance_Position(LineData);

                if(Mode == 1){
                    // if robot is too far right of line (Position > 0)
                    // increase change
                    UR = PWMNOMINAL - Kp4 * Position;
                    UL = PWMNOMINAL + Kp4 * Position;

                    // limit change to within swing
                    if(UR < PWMIN){
                        UR = PWMIN;
                    }
                    if(UL < PWMIN){
                        UL = PWMIN;
                    }
                    if(UR > PWMAX){
                        UR = PWMAX;
                    }
                    if(UL > PWMAX){
                        UL = PWMAX;
                    }

                    // update duty cycle based on porportional control
                    Motor_Forward(UL, UR);
                    ControllerFlag = 1;
        }
        // Use Reflectance_Position() to find position

        }
    }
}


void program1(void){
    DisableInterrupts();
    Clock_Init48MHz();
    LaunchPad_Init();
    Bump_Init();
    Reflectance_Init();
    Motor_Init();
    Tachometer_Init();
    TimerA1_Init(&Controller4,500);
    Motor_Stop();

   // LCDClear3();
    EnableInterrupts();
    Bump = 0;
    UR = UL = PWMNOMINAL;    // reset parameters
    Mode = 1;
    ControllerFlag = 0;
    Time = 0;
    while(1){

        if(Exit ==1){
            Mode = 0;
            break;
        }
        if(ControllerFlag == 1){ // 100 Hz , not real time
         //  LCDOut3();
           ControllerFlag = 0;
       }
    }
    while(1){

        Clock_Delay1ms(500); LaunchPad_Output(0); // off
        Clock_Delay1ms(500); LaunchPad_Output(1); // red
        Clock_Delay1ms(500); LaunchPad_Output(0); // off
        Clock_Delay1ms(500); LaunchPad_Output(4); // blue
    }
}
int Kp5 = 7;
int rightLast = -1;
int Count = 0;
#define PWMNOMINAL2 3000

#define SWING2 500
#define PWMIN2 (PWMNOMINAL2-SWING2)
#define PWMAX2 (PWMNOMINAL2+SWING2)
void lineClassify(int position){
    if(position >= 247){
        Mode = 2;
        Time = 0;
        tachStep = -1;
        Count = 0;
    }
    else if(position == 0){
        Mode = 3;
        Time = 0;
        tachStep = -1;
    }
    else if(position == 219 || position == 155 || position == 187 || position == 189 || position == 153 || position == 139){
        Exit = 1;
        Mode = 0;
        Time = 0;

    }
    else{
        Count  = 0;
        Mode = 1;
    }

}

void Controller5(void){
    Time = Time + 1;
    Tachometer_Get(&LeftTach[j], &LeftDir, &LeftSteps, &RightTach[j], &RightDir, &RightSteps);
    if (Mode == 0){
        Exit = 1;
    }
    else if(Mode == 2){
            if(tachStep == -1){
                Motor_Forward(UL,UR);
                if(Time > 350){
                    tachStep = 0;
                }
            }else{
                Motor_Stop();
                if(rightLast == -1){
                    rightLast = RightSteps;
                }
                tachStep = RightSteps - rightLast;
                turn =  (70 * tachStep) / 150;
                if( turn >= 90){
                    Mode = 1;
                    Time = 0;
                    rightLast = -1;
                    tachStep = -1;

                }else{
                UR = PWMNOMINAL2;
                UL = PWMNOMINAL2;
                Motor_Right(UL, UR);
                Mode = 2;
                }
                ControllerFlag = 1;
                Time = 0;
                Count = 0;
            }
        }
        else if(Mode == 3){
            Motor_Stop();
            if(rightLast == -1){
               rightLast = RightSteps;
            }
            tachStep = RightSteps - rightLast;
            turn =  (70 * tachStep) / 150;
            if( turn >= 180){
              Mode = 1;
              Time = 0;
              rightLast = -1;
              tachStep = -1;
            }else{
             UR = PWMNOMINAL2;
             UL = PWMNOMINAL2;
             Motor_Right(UL, UR);
             Mode = 3;
          }
          ControllerFlag = 1;
          Time = 0;
        }
     else{
        if(Time == 11){
           Reflectance_Start();
        }
        if(Time == 12){
            LineData = Reflectance_End();
            LineData = ~LineData;
            Time = 0;
            Position = Reflectance_Position(LineData);
            lineClassify(LineData);
            if(Mode == 1){
              // if robot is too far right of line (Position > 0)
             // increase change
             UR = PWMNOMINAL2 - Kp5 * Position;
             UL = PWMNOMINAL2 + Kp5 * Position;

             // limit change to within swing
             if(UR < PWMIN2){
                UR = PWMIN2;
             }
             if(UL < PWMIN2){
                UL = PWMIN2;
             }
             if(UR > PWMAX2){
                UR = PWMAX2;
             }
             if(UL > PWMAX2){
                UL = PWMAX2;
             }

              // update duty cycle based on porportional control
              Motor_Forward(UL, UR);
              ControllerFlag = 1;
             }
        }
    }
}

void program2(void){
    DisableInterrupts();
    Clock_Init48MHz();
    LaunchPad_Init();
    Bump_Init();
    Reflectance_Init();
    Motor_Init();
    Tachometer_Init();
    TimerA1_Init(&Controller5,500);
    Motor_Stop();
      // LCDClear3();
    EnableInterrupts();
    Bump = 0;
    UR = UL = PWMNOMINAL2;    // reset parameters
    Mode = 1;
    ControllerFlag = 0;
    Time = 0;
    leftLast = -1;
    tachStep = -1;
    Hit = 0;
    Exit = 0;
    j =0;
    turn = 0;
    while(1){
        if(Exit == 1){
            Mode = 0;
            Motor_Stop();
            break;
        }
        if(ControllerFlag == 1){
            ControllerFlag = 0;
        }
    }
    while(1){
        Motor_Stop();
        Clock_Delay1ms(500); LaunchPad_Output(0); // off
        Clock_Delay1ms(500); LaunchPad_Output(1); // red
        Clock_Delay1ms(500); LaunchPad_Output(0); // off
        Clock_Delay1ms(500); LaunchPad_Output(4); // blue
        }
}
#define PWMNOMINAL3 7500

#define SWING3 1000
#define PWMIN3 (PWMNOMINAL3-SWING3)
#define PWMAX3 (PWMNOMINAL3+SWING3)
#define DESIRED_SPEED 100


int32_t ActualSpeedL, ActualSpeedR;        // Actual speed
int32_t ErrorL, ErrorR;
int i =0;
int Kp6 = 3000;
void Controller6(void){
    if(Bump_Read()!= 0x3f){ // collision
           Mode = 2;
        }
    Time = Time + 1;
    Tachometer_Get(&LeftTach[i], &LeftDir, &LeftSteps, &RightTach[i], &RightDir, &RightSteps);
    if (Mode == 0){
        Exit = 1;
    }
    else if(Mode == 2){
            if(tachStep == -300){
                Motor_Backward(PWMNOMINAL3, PWMNOMINAL3);
                 if(Time >= 300){
                  tachStep = 0;
                }
            }else{
                Motor_Stop();
                if(rightLast == -1){
                    rightLast = RightSteps;
                }
                tachStep = RightSteps - rightLast;
                turn =  (70 * tachStep) / 150;
                if( turn >= 85){
                    Mode = 1;
                    Time = 0;
                    rightLast = -1;
                    tachStep = -1;

                }else{
                UR = PWMNOMINAL3;
                UL = PWMNOMINAL3;
                Motor_Right(UL, UR);
                Mode = 2;
                }
                ControllerFlag = 1;
                Time = 0;
                Count = 0;
            }
        }

     else{
         if(Mode){
               // pull tachometer information
              // use average of ten tachometer values to determine
               // actual speed (similar to Lab16)
                 i = i + 1;
                 if(i >= TACHBUFF){
                   i = 0;
                   ActualSpeedL = 2000000/avg(LeftTach, TACHBUFF);
                   ActualSpeedR = 2000000/avg(RightTach, TACHBUFF);
                   ErrorL = DESIRED_SPEED - ActualSpeedL;
                   ErrorR = DESIRED_SPEED - ActualSpeedR;

                   // use proportional control to update duty cycle
                   // LeftDuty = LeftDuty + Kp*LeftError
                   UR = UR + ((Kp6*ErrorR)/100);
                   UL = UL + ((Kp6*ErrorL)/100);

                   // check min/max duty values
                   if(PWMIN3 > UR){
                       UR = PWMIN3;
                   }
                   if(PWMAX3 < UR){
                       UR = PWMAX3;
                   }
                   if(PWMIN3 > UL){
                       UL = PWMIN3;
                   }
                   if(PWMAX3 < UL){
                       UL = PWMAX3;
                   }

                   // update motor values
                    Motor_Forward(UL, UR);
                   ControllerFlag = 1;
                 }
             }
    }
}
void program3(void){
    DisableInterrupts();
    Clock_Init48MHz();
    LaunchPad_Init();
    Bump_Init();
    Reflectance_Init();
    Motor_Init();
    Tachometer_Init();
    TimerA1_Init(&Controller6,500);
    Motor_Stop();
      // LCDClear3();
    EnableInterrupts();
    Bump = 0;
    UR = UL = PWMNOMINAL3;    // reset parameters
    Mode = 1;
    ControllerFlag = 0;
    Time = 0;
    rightLast = -1;
    tachStep = -300;
    Hit = 0;
    Exit = 0;
    i =0;
    turn = 0;
    while(1){
        if(Exit == 1){
            Mode = 0;
            Motor_Stop();
            break;
        }
        if(ControllerFlag == 1){
            ControllerFlag = 0;
        }
    }
    while(1){
        Motor_Stop();
        Clock_Delay1ms(500); LaunchPad_Output(0); // off
        Clock_Delay1ms(500); LaunchPad_Output(1); // red
        Clock_Delay1ms(500); LaunchPad_Output(0); // off
        Clock_Delay1ms(500); LaunchPad_Output(4); // blue
        }
}

#define TOOCLOSE 110
#define DESIRED_DIST 172
#define TOOFAR 230


#define PWMNOMINAL4 7500
#define SWING4 1000
#define PWMIN4 (PWMNOMINAL4-SWING4)
#define PWMAX4 (PWMNOMINAL4+SWING4)

volatile uint32_t nr, nc, nl; // raw distance values
int32_t Left, Center, Right; // IR distances in mm

volatile uint32_t ADCflag; // Set every 500us on ADC sample
int32_t DataBuffer[5];
int32_t SetPoint = 172;
uint32_t PosError;
int32_t Error2;

void Pause3(void){
    int j;
  while(Bump_Read() != 0x3f){ // wait for release
    Clock_Delay1ms(200); LaunchPad_Output(0); // off
    Clock_Delay1ms(200); LaunchPad_Output(1); // red
  }
  while(Bump_Read()== 0x3f){// wait for touch
    Clock_Delay1ms(100); LaunchPad_Output(0); // off
    Clock_Delay1ms(100); LaunchPad_Output(3); // red/green
  }
  while(Bump_Read() != 0x3f){ // wait for release
    Clock_Delay1ms(100); LaunchPad_Output(0); // off
    Clock_Delay1ms(100); LaunchPad_Output(4); // blue
  }
  for(j=1000;j>100;j=j-200){
    Clock_Delay1ms(j); LaunchPad_Output(0); // off
    Clock_Delay1ms(j); LaunchPad_Output(2); // green
  }
  // restart Jacki
  UR = UL = PWMNOMINAL4;    // reset parameters
  Mode = 1;
  ControllerFlag = 0;
  Time = 0;
}

scenario_t wall_state;

#define  inLeftState 0
#define  inStraightState  1
#define  transitionState  2
#define  inBlockedState  4

int exe = 1;
int Kp2 = 8;
void LCDClear2(void){
  Nokia5110_Init();
  Nokia5110_Clear(); // erase entire display
  Nokia5110_OutString("17: control");
  Nokia5110_SetCursor(0,1); Nokia5110_OutString("IR distance");
  Nokia5110_SetCursor(0,2); Nokia5110_OutString("L= "); Nokia5110_OutSDec(0); Nokia5110_OutString(" mm");
  Nokia5110_SetCursor(0,3); Nokia5110_OutString("C= "); Nokia5110_OutSDec(0); Nokia5110_OutString(" mm");
  Nokia5110_SetCursor(0,4); Nokia5110_OutString("R= "); Nokia5110_OutSDec(0); Nokia5110_OutString(" mm");
  Nokia5110_SetCursor(0,5); Nokia5110_OutString("E= "); Nokia5110_OutSDec(0); Nokia5110_OutString(" mm");
}

void LCDOut2(void){
  Nokia5110_SetCursor(3,2); Nokia5110_OutSDec(Left);
  Nokia5110_SetCursor(3,3); Nokia5110_OutSDec(Center);
  Nokia5110_SetCursor(3,4); Nokia5110_OutSDec(Right);
  Nokia5110_SetCursor(3,5); Nokia5110_OutSDec(Error);
  // left
  if(Time%5 == 0){
      UART0_OutUDec5(Left);UART0_OutString(" mm,");
      UART0_OutUDec5(Center);UART0_OutString(" mm,");
      UART0_OutUDec5(Right);UART0_OutString(" mm,");
      UART0_OutUDec5(UR);UART0_OutString(" %,");
      UART0_OutUDec5(UL);UART0_OutString(" %,");
      if(Error < 0){
          PosError = Error*(-1);
          UART0_OutString("-");UART0_OutUDec5(PosError);UART0_OutString("\n");
      }
      else{
          UART0_OutUDec5(Error);UART0_OutString("\n");
      }

  }
}

void IRsampling(void){
    uint32_t raw17, raw12, raw16;
    ADC_In17_12_16(&raw17, &raw12, &raw16);
    nr = LPF_Calc(raw17);
    nc = LPF_Calc2(raw12);
    nl = LPF_Calc3(raw16);
    Left = LeftConvert(nl);
    Center = CenterConvert(nc);
    Right = RightConvert(nr);
    ADCflag = 1;
}

/*
* Proportional controller to keep robot in
* center of two walls using IR sensors.
*/
void SysTick_Handler(void){
    Tachometer_Get(&LeftTach[j], &LeftDir, &LeftSteps, &RightTach[j], &RightDir, &RightSteps);
    Time = Time + 1;
    if(Mode){
        wall_state = Classify(Left, Center, Right);

        if(wall_state == LeftTurn && exe != transitionState && exe != inBlockedState){
            exe = inLeftState;
        }
        if(wall_state == Blocked && exe != transitionState && exe != inLeftState){
            exe = inBlockedState;
        }
        if(exe == inBlockedState){
            wall_state = Blocked;
        }
        if(exe == inLeftState){
            wall_state = LeftTurn;
        }
        if(exe == transitionState){
            exe = inStraightState;
            wall_state = Straight;
        }
        if(exe == inStraightState){
                      wall_state = Straight;
        }

        if(wall_state == Straight && exe == inStraightState){
            // Determine set point
           if(Left > DESIRED_DIST && Right > DESIRED_DIST){
              SetPoint = (Left + Right) / 2;
             }
             else{
               SetPoint = 172;
             }
             // set error based off set point
             if(Left < Right){
                Error2 = Left - SetPoint;
             }else{
               Error2 = SetPoint - Right;
              }

             // update duty cycle based on proportional control
             UR = UR + Kp2 * Error2;
             UL = UL - Kp2 * Error2;
             // check to ensure not too big of a swing
            if(UR < PWMIN4){
               UR = PWMIN4;
            }
            if(UR > PWMAX4){
               UR = PWMAX4;
            }
            if(UL < PWMIN4){
               UL = PWMIN4;
            }
            if(UL > PWMAX4){
               UL = PWMAX4;
            }
            // update motor values
            Motor_Forward(UL, UR);
            ControllerFlag = 1;
            Time = 0;
        }
        else if(wall_state == LeftTurn && exe == inLeftState){
            if(tachStep == -300){
              Motor_Forward(UL,UR);
              if(Time > 300){
                 tachStep = 0;
              }

            }else{
            Motor_Stop();
            if(rightLast == -1){
               rightLast = RightSteps;
            }
            tachStep = RightSteps - rightLast;
            turn =  (70 * tachStep) / 150;
            if( turn >= 90){
                exe = transitionState;
                rightLast = -1;
                tachStep = -300;
                Time = 0;
                turn = 0;
            }else{
              UR = PWMNOMINAL4;
              UL = PWMNOMINAL4;
              Motor_Right(UL, UR);
            }
             ControllerFlag = 1;
            }
         }
        else if(wall_state == Blocked && exe == inBlockedState){
            Motor_Stop();
            if(rightLast == -1){
                rightLast = RightSteps;
            }
            tachStep = RightSteps - rightLast;
            turn =  (70 * tachStep) / 150;
            if( turn >= 180){
                exe = transitionState;
                Time = 0;
                rightLast = -1;
                tachStep = -300;
                turn = 0;
             }else{
               UR = PWMNOMINAL4;
               UL = PWMNOMINAL4;
               Motor_Right(UL, UR);

             }
              ControllerFlag = 1;
              Time = 0;
        }

    }
}

// proportional control, wall distance
void program4(void){
    uint32_t raw17,raw12,raw16;
    DisableInterrupts();
    Clock_Init48MHz();
    LaunchPad_Init();
    Bump_Init();
    Motor_Init();
    Tachometer_Init();
    // user TimerA1 to sample the IR sensors at 2000 Hz
    // replace this line with a call to TimerA1_Init()
    TimerA1_Init(&IRsampling, 250);
    Motor_Stop();
    LCDClear2();
    Mode = 0;
    UR = UL = PWMNOMINAL4;
    ADCflag = ControllerFlag = 0;   // semaphores
    rightLast = -1;
    tachStep = -300;
    j = 0;
    exe = inStraightState;
    Left= 600;
    Center = 600;
    Right = 600;
    turn = 0;
    ADC0_InitSWTriggerCh17_12_16();   // initialize channels 17,12,16
    ADC_In17_12_16(&raw17,&raw12,&raw16);  // sample
    LPF_Init(raw17,64);     // P9.0/channel 17
    LPF_Init2(raw12,64);    // P4.1/channel 12
    LPF_Init3(raw16,64);    // P9.1/channel 16

    // user SysTick to run the controller at 100 Hz with a priority of 2
    // replace this line with a call to SysTick_Init()
    SysTick_Init(480000,2);
    Pause3();

    EnableInterrupts();
    Motor_Forward(UL,UR);
    while(1){
        if(Bump_Read() != 0x3f){ // collision
            Mode = 0;
            Motor_Stop();
            Pause3();
        }
        if(ControllerFlag == 1){ // 100 Hz, not real time
            LCDOut2();
            ControllerFlag = 0;
        }
    }
}
 int main(void){
  //  Program17_1();
  //  Program17_2();
  // Program17_3();
  //  program1();
   // program2();
  //   program3();
    program4();
}
