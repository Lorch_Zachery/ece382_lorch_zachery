// Lab17_Control.c
// Runs on MSP432
// Implementation of the control system.
// Daniel and Jonathan Valvano
// September 12, 2017

/* This example accompanies the books
   "Embedded Systems: Introduction to the MSP432 Microcontroller",
       ISBN: 978-1512185676, Jonathan Valvano, copyright (c) 2017
   "Embedded Systems: Real-Time Interfacing to the MSP432 Microcontroller",
       ISBN: 978-1514676585, Jonathan Valvano, copyright (c) 2017
   "Embedded Systems: Real-Time Operating Systems for ARM Cortex-M Microcontrollers",
       ISBN: 978-1466468863, , Jonathan Valvano, copyright (c) 2017
 For more information about my classes, my research, and my books, see
 http://users.ece.utexas.edu/~valvano/

Simplified BSD License (FreeBSD License)
Copyright (c) 2017, Jonathan Valvano, All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are
those of the authors and should not be interpreted as representing official
policies, either expressed or implied, of the FreeBSD Project.
*/

#include <stdint.h>
#include "msp.h"
#include "../inc/Clock.h"
#include "../inc/CortexM.h"
#include "../inc/PWM.h"
#include "../inc/LaunchPad.h"
#include "../inc/UART0.h"
#include "../inc/Motor.h"
#include "../inc/Bump.h"
#include "../inc/ADC14.h"
#include "../inc/TimerA1.h"
#include "../inc/IRDistance.h"
#include "../inc/Nokia5110.h"
#include "../inc/LPF.h"
#include "../inc/FlashProgram.h"
#include "../inc/SysTickInts.h"
#include "../inc/Tachometer.h"
#include "../inc/Reflectance.h"


/**************Initial values used for all programs******************/
#define PWMNOMINAL 5000
#define SWING 1000
#define PWMIN (PWMNOMINAL-SWING)
#define PWMAX (PWMNOMINAL+SWING)

volatile uint32_t ControllerFlag; // set every 10ms on controller execution

int32_t Mode = 0;
int32_t UL, UR;             // Controller output PWM duty 2 to 14,998
uint32_t Time; // in 0.01 sec

// proportional controller gain 
// experimentally determine value that creates a stable system
// may want a different gain for each program
int32_t Kp= 50;

/**************Functions used by all programs***********************/
void Pause3(void){
    int j;
  while(Bump_Read() != 0x3f){ // wait for release
    Clock_Delay1ms(200); LaunchPad_Output(0); // off
    Clock_Delay1ms(200); LaunchPad_Output(1); // red
  }
  while(Bump_Read()== 0x3f){// wait for touch
    Clock_Delay1ms(100); LaunchPad_Output(0); // off
    Clock_Delay1ms(100); LaunchPad_Output(3); // red/green
  }
  while(Bump_Read() != 0x3f){ // wait for release
    Clock_Delay1ms(100); LaunchPad_Output(0); // off
    Clock_Delay1ms(100); LaunchPad_Output(4); // blue
  }
  for(j=1000;j>100;j=j-200){
    Clock_Delay1ms(j); LaunchPad_Output(0); // off
    Clock_Delay1ms(j); LaunchPad_Output(2); // green
  }
  // restart Jacki
  UR = UL = PWMNOMINAL;    // reset parameters
  Mode = 1;
  ControllerFlag = 0;
  Time = 0;
}

/**************Program17_1******************************************/
#define DESIRED_SPEED 100
#define TACHBUFF 10                      // number of elements in tachometer array

int32_t ActualSpeedL, ActualSpeedR;   	 // Actual speed
int32_t ErrorL, ErrorR;     			 // X* - X'
uint16_t LeftTach[TACHBUFF];             // tachometer period of left wheel (number of 0.0833 usec cycles to rotate 1/360 of a wheel rotation)
enum TachDirection LeftDir;              // direction of left rotation (FORWARD, STOPPED, REVERSE)
int32_t LeftSteps;                       // number of tachometer steps of left wheel (units of 220/360 = 0.61 mm traveled)
uint16_t RightTach[TACHBUFF];            // tachometer period of right wheel (number of 0.0833 usec cycles to rotate 1/360 of a wheel rotation)
enum TachDirection RightDir;             // direction of right rotation (FORWARD, STOPPED, REVERSE)
int32_t RightSteps;                      // number of tachometer steps of right wheel (units of 220/360 = 0.61 mm traveled)

int i = 0;

void LCDClear1(void){
    Nokia5110_Init();
    Nokia5110_Clear();
    Nokia5110_OutString("Desired(RPM)L     R     Actual (RPM)L     R     Error(RPM)  L     R     ");
}
void LCDOut1(void){
    Nokia5110_SetCursor(1, 1);         // one leading space, second row
    Nokia5110_OutSDec1(DESIRED_SPEED);
    Nokia5110_SetCursor(7, 1);         // seven leading spaces, second row
    Nokia5110_OutSDec1(DESIRED_SPEED);
    Nokia5110_SetCursor(1, 3);       // one leading space, fourth row
    Nokia5110_OutUDec(ActualSpeedL);
    Nokia5110_SetCursor(7, 3);       // seven leading spaces, fourth row
    Nokia5110_OutUDec(ActualSpeedR);
    Nokia5110_SetCursor(1, 5);       // zero leading spaces, sixth row
    Nokia5110_OutSDec1(ErrorL);
    Nokia5110_SetCursor(7, 5);       // six leading spaces, sixth row
    Nokia5110_OutSDec1(ErrorR);
}

// ------------avg------------
// Simple math function that returns the average
// value of an array.
// Input: array is an array of 16-bit unsigned numbers
//        length is the number of elements in 'array'
// Output: the average value of the array
// Note: overflow is not considered
uint16_t avg(uint16_t *array, int length){
  int i;
  uint32_t sum = 0;
  for(i=0; i<length; i=i+1){
    sum = sum + array[i];
  }
  return (sum/length);
}

//**************************************************
// Proportional controller to drive straight with no
// sensor input
void Controller1(void){

    if(Mode){
		// pull tachometer information
        Tachometer_Get(&LeftTach[i], &LeftDir, &LeftSteps, &RightTach[i], &RightDir, &RightSteps);

		// use average of ten tachometer values to determine 
		// actual speed (similar to Lab16)
        i = i + 1;
        if(i >= TACHBUFF){
          i = 0;
          ActualSpeedL = 2000000/avg(LeftTach, TACHBUFF);
          ActualSpeedR = 2000000/avg(RightTach, TACHBUFF);
          ErrorL = DESIRED_SPEED - ActualSpeedL;
          ErrorR = DESIRED_SPEED - ActualSpeedR;

			// use proportional control to update duty cycle
			// LeftDuty = LeftDuty + Kp*LeftError
			UR = UR + ((Kp*ErrorL)/10);
			UL = UL + ((Kp*ErrorR)/10);

			// check min/max duty values
			if(PWMIN > UR){
			    UR = PWMIN;
			}
			if(PWMAX < UR){
			    UR = PWMAX;
			}
			if(PWMIN > UL){
			    UL = PWMIN;
			}
			if(PWMAX < UL){
			    UL = PWMAX;
			}

			// update motor values
            Motor_Forward(UL, UR);
			ControllerFlag = 1;
        }
    }
}

// go straight with no sensor input
void Program17_1(void){
  DisableInterrupts();
// initialization
  Clock_Init48MHz();
  LaunchPad_Init();
  Bump_Init();
  Tachometer_Init();
  Motor_Init();
  
  // user TimerA1 to run the controller at 100 Hz
  // replace this line with a call to TimerA1_Init()
  TimerA1_Init(&Controller1,10);
  Motor_Stop();
  UR = UL = PWMNOMINAL;
  EnableInterrupts();
  LCDClear1();
  ControllerFlag = 0;
  Pause3();

	while(1){
		if(Bump_Read() != 0x3f){
			Mode = 0;
			Motor_Stop();
			Pause3();
		}
		if(ControllerFlag == 1){
			LCDOut1();
			ControllerFlag = 0;
		}
  }
}

int main(void){
    Program17_1();
//    Program17_2();
//    Program17_3();
}
