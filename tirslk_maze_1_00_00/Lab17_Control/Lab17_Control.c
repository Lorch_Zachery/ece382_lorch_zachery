// Lab17_Control.c
// Runs on MSP432
// Implementation of the control system.
// Daniel and Jonathan Valvano
// September 12, 2017

/* This example accompanies the books
   "Embedded Systems: Introduction to the MSP432 Microcontroller",
       ISBN: 978-1512185676, Jonathan Valvano, copyright (c) 2017
   "Embedded Systems: Real-Time Interfacing to the MSP432 Microcontroller",
       ISBN: 978-1514676585, Jonathan Valvano, copyright (c) 2017
   "Embedded Systems: Real-Time Operating Systems for ARM Cortex-M Microcontrollers",
       ISBN: 978-1466468863, , Jonathan Valvano, copyright (c) 2017
 For more information about my classes, my research, and my books, see
 http://users.ece.utexas.edu/~valvano/

Simplified BSD License (FreeBSD License)
Copyright (c) 2017, Jonathan Valvano, All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are
those of the authors and should not be interpreted as representing official
policies, either expressed or implied, of the FreeBSD Project.
*/

#include <stdint.h>
#include "msp.h"
#include "../inc/Clock.h"
#include "../inc/CortexM.h"
#include "../inc/PWM.h"
#include "../inc/LaunchPad.h"
#include "../inc/UART0.h"
#include "../inc/Motor.h"
#include "../inc/Bump.h"
#include "../inc/ADC14.h"
#include "../inc/TimerA1.h"
#include "../inc/IRDistance.h"
#include "../inc/Nokia5110.h"
#include "../inc/LPF.h"
#include "../inc/FlashProgram.h"
#include "../inc/SysTickInts.h"
#include "../inc/Tachometer.h"
#include "../inc/Reflectance.h"
#include "../inc/Classifier.h"

/*
Varibles and Functions used by all programs
*/

volatile uint32_t ControllerFlag; // set every 10ms on controller execution

int32_t Mode = 0; // Mode variable to change which Mode the controller will exicute
int32_t UL, UR;             // Controller output PWM duty 2 to 14,998
uint32_t Time; // in 0.01 sec
#define WAIT -300

/*
avg - returns unisigned int16
	gets the average of an array and given lenght
parameters - takes in array and the lenght of the array
*/

uint16_t avg(uint16_t *array, int length){
  int i;
  uint32_t sum = 0;
  for(i=0; i<length; i=i+1){
    sum = sum + array[i];
  }
  return (sum/length);
}

/*
Constants for program 1, challenge 1
*/
#define PWMNOMINAL 3000

#define SWING 750
#define PWMIN (PWMNOMINAL-SWING)
#define PWMAX (PWMNOMINAL+SWING)
#define TACHBUFF 10
//used to get information from the tachometer
uint16_t LeftTach[TACHBUFF];            // tachometer period of left wheel (number of 0.0833 usec cycles to rotate 1/360 of a wheel rotation)
enum TachDirection LeftDir;
int32_t LeftSteps;
uint16_t RightTach[TACHBUFF];
enum TachDirection RightDir;
int32_t RightSteps;
uint8_t LineData = 24;       // direct measure from line sensor
int32_t Position;      // position in 0.1mm relative to center of line

//globals to run program 1
int Kp4 = 20; // Kp is set to 20 to make position affect the speed of each wheel
int leftLast = -1;
int tachStep = WAIT;
int Hit = 0;
int Exit = 0;
int j =0;
int turn = 0;

//Controller for program 1
void Controller4(void){
	//if there is a collision
    if(Bump_Read()!= 0x3f){ // collision
       Motor_Stop();
	   //if the Hit semaphore has been set program will exit, Hit semaphore checks to see 
	   //if the robot has hit a wall yet, because the robot must hit a wall twice
      if(Hit == 1){
          Exit = 1;
          Motor_Stop();
     }else{
         Mode = 2;
         Time = 0;
     }
    }

	//updating time every time the controller is run
    Time = Time + 1;
	//checking tachometer everytime the controller is run 
    Tachometer_Get(&LeftTach[j], &LeftDir, &LeftSteps, &RightTach[j], &RightDir, &RightSteps);
	
	//if controll is in mode 2
    if(Mode == 2){
		//if tachStep has not yet been set, the robot will move backwards for a set time to get 
		//away from the wall
        if(tachStep == WAIT){
            Motor_Backward(PWMNOMINAL, PWMNOMINAL);
            if(Time >= 300){
                tachStep = 0;

            }
        }
		//after the robot has backed away from the wall robot does a 180 degree turn using 
		//the tachometer
        else{
			//setting the current tachometer reading 
            if(leftLast == -1){
                leftLast = LeftSteps;
            }
			//getting the amount of steps taken
            tachStep = LeftSteps - leftLast;
			//checking to see how many degrees the wheel has moved
            turn =  (70 * tachStep) / 150;
			//if the wheel has moved 180 degrees or more, mode changes 
            if( turn >= 180){
                Mode = 1;
                Time = 0;
	
            }
			//if turn is not complete yet, keep turning
			else{
            UR = PWMNOMINAL + 2000;
            UL = PWMNOMINAL + 2000;
            Motor_Left(UL, UR);
            Mode = 2;
            }
			//Hit will be set to 1 once this mode is enter because it means the wall has 
			//already been hit once
            Hit = 1;

        }
    }
	//if not in mode 2 
    else{
		//start line reading 
        if(Time == 11){
            Reflectance_Start();
        }
		//get line reading data
        if(Time == 12){
			//invert data due to following a white line and not a black line
            LineData = ~Reflectance_End();
            Time = 0;
			//getting position on the line
            Position = Reflectance_Position(LineData);
				//Enter mode 1, line following
                if(Mode == 1){
                    // if robot is too far right of line (Position > 0)
                    // increase change
                    UR = PWMNOMINAL - Kp4 * Position;
                    UL = PWMNOMINAL + Kp4 * Position;

                    // limit change to within swing
                    if(UR < PWMIN){
                        UR = PWMIN;
                    }
                    if(UL < PWMIN){
                        UL = PWMIN;
                    }
                    if(UR > PWMAX){
                        UR = PWMAX;
                    }
                    if(UL > PWMAX){
                        UL = PWMAX;
                    }

                    // update duty cycle based on porportional control
                    Motor_Forward(UL, UR);
                    ControllerFlag = 1;
				}
			}
	}
}
/*
Program 1, has the robot follow a white line till it hits a wall
The robot will then turn around and go until it hits the wall it started
near
*/

void program1(void){
	//initialize all the functionality used on the robot
    DisableInterrupts();
    Clock_Init48MHz();
    LaunchPad_Init();
    Bump_Init();
    Reflectance_Init();
    Motor_Init();
    Tachometer_Init();
	//giving timerA1 the control for following the white line
    TimerA1_Init(&Controller4,500);
    Motor_Stop();
    EnableInterrupts();
	//Setting globals and reseting them 
    UR = UL = PWMNOMINAL;    
    Mode = 1;
    ControllerFlag = 0;
    Time = 0;
    while(1){
		//If Exit has been reach while loop breaks
        if(Exit ==1){
            Mode = 0;
            break;
        }
		///changing controller flag to update the robot
        if(ControllerFlag == 1){ // 100 Hz , not real time
           ControllerFlag = 0;
       }
    }
	//Blinks LEDs red and blue to indicate a finish
    while(1){

        Clock_Delay1ms(500); LaunchPad_Output(0); // off
        Clock_Delay1ms(500); LaunchPad_Output(1); // red
        Clock_Delay1ms(500); LaunchPad_Output(0); // off
        Clock_Delay1ms(500); LaunchPad_Output(4); // blue
    }
}

//Globals for program 2
int Kp5 = 7;
int rightLast = -1;


#define PWMNOMINAL2 3000

#define SWING2 500
#define PWMIN2 (PWMNOMINAL2-SWING2)
#define PWMAX2 (PWMNOMINAL2+SWING2)

/*
lineClassify - returns void
	function classifies the line reading to update the position 
	checks if the robot is off the line, at a left turn, or at the ending sequence
parameters- takings in binary reading of the line sensor
*/
void lineClassify(int position){
	//at a leftTurn point
    if(position >= 247){
        Mode = 2;
        Time = 0;
        tachStep = -1;
       
    }
	//off of the line 
    else if(position == 0){
        Mode = 3;
        Time = 0;
        tachStep = -1;
    }
	//at the end sequence
    else if(position == 219 || position == 155 || position == 187 || position == 189 || position == 153 || position == 139){
        Exit = 1;
        Mode = 0;
        Time = 0;

    }
	//normal line following
    else{
        
        Mode = 1;
    }

}

//Controller for program 2
void Controller5(void){
	//updating Time and tachometer everytime the controller is used
    Time = Time + 1;
    Tachometer_Get(&LeftTach[j], &LeftDir, &LeftSteps, &RightTach[j], &RightDir, &RightSteps);
	//if in Mode 0, the robot has reached the end sequence, Exit the program
    if (Mode == 0){
        Exit = 1;
    }
	//if program in Mode 2, left turn
    else if(Mode == 2){
		//moves the robot forward to allow for the robot to clear the walls
            if(tachStep == -1){
                Motor_Forward(UL,UR);
                if(Time > 350){
                    tachStep = 0;
                }
				
            }
			//after robot has moved forward a little exicute left turn
			else{
                Motor_Stop();
				//getting the current tachometer reading
                if(rightLast == -1){
                    rightLast = RightSteps;
                }
				//getting the degrees the robot has turned 
                tachStep = RightSteps - rightLast;
                turn =  (70 * tachStep) / 150;
				//if the robot has turned 90 degrees or more stop turning
				//and go back to normal line following
                if( turn >= 90){
					//setting mode to normal line follow 
                    Mode = 1;
					//reseting globals
                    Time = 0;
                    rightLast = -1;
                    tachStep = -1;

                }
				//if the robot has not finished the turn, keep turning
				else{
                UR = PWMNOMINAL2;
                UL = PWMNOMINAL2;
                Motor_Right(UL, UR);
                Mode = 2;
                }
				//reseting globals
                ControllerFlag = 1;
                Time = 0;
                 
            }
    }
	//Mode 3, off of the line, so robot needs to turn around
    else if(Mode == 3){
        Motor_Stop();
		//getting last tachometer reading 
        if(rightLast == -1){
			rightLast = RightSteps;
        }
		//getting the degrees the robot has turned
		tachStep = RightSteps - rightLast;
		turn =  (70 * tachStep) / 150;
		//if the robot has turned around, reset to line follow mode and reset globals
		if( turn >= 180){
			Mode = 1;
			Time = 0;
			rightLast = -1;
			tachStep = -1;
        }
		//if the turn is not complete keep turning
		else{
			UR = PWMNOMINAL2;
            UL = PWMNOMINAL2;
            Motor_Right(UL, UR);
            Mode = 3;
        }
		//reseting globals
        ControllerFlag = 1;
        Time = 0;
    }
    else{
		//starting line reading 
        if(Time == 11){
			Reflectance_Start();
        }
		//gettting line reading data
        if(Time == 12){
            LineData = Reflectance_End();
			//inverting line data because follow a white line 
            LineData = ~LineData;
			//reseting time 
            Time = 0;
			//getting current postion of the robot on the line
            Position = Reflectance_Position(LineData);
			//classifying the linedata into the scenarios 
            lineClassify(LineData);
			
			//Mode 1, normal line following
            if(Mode == 1){
				  // if robot is too far right of line (Position > 0)
				 // increase change
				 UR = PWMNOMINAL2 - Kp5 * Position;
				 UL = PWMNOMINAL2 + Kp5 * Position;

				 // limit change to within swing
				 if(UR < PWMIN2){
					UR = PWMIN2;
				 }
				 if(UL < PWMIN2){
					UL = PWMIN2;
				 }
				 if(UR > PWMAX2){
					UR = PWMAX2;
				 }
				 if(UL > PWMAX2){
					UL = PWMAX2;
				 }

				  // update duty cycle based on porportional control
				  Motor_Forward(UL, UR);
				  ControllerFlag = 1;
             }
        }
    }
}

/*
program 2 has a robot follow a white line through a maze, this program only preforms
left turns to get the robot out of the maze
*/
void program2(void){
	//initializing all the functionality used by the robot
    DisableInterrupts();
    Clock_Init48MHz();
    LaunchPad_Init();
    Bump_Init();
    Reflectance_Init();
    Motor_Init();
    Tachometer_Init();
	//giving TimerA1 the controller for this program 
    TimerA1_Init(&Controller5,500);
    Motor_Stop();
    EnableInterrupts();
	//setting globals
    UR = UL = PWMNOMINAL2;    // reset parameters
    Mode = 1;
    ControllerFlag = 0;
    Time = 0;
    rightLast = -1;
    tachStep = -1;
    Exit = 0;
    j =0;
    turn = 0;
    while(1){
		//if exit has been reached exit while loop
        if(Exit == 1){
            Mode = 0;
            Motor_Stop();
            break;
        }
		//sets controller flag
        if(ControllerFlag == 1){
            ControllerFlag = 0;
        }
    }
	//blinks LEds red and blue to indicate finished 
    while(1){
        Motor_Stop();
        Clock_Delay1ms(500); LaunchPad_Output(0); // off
        Clock_Delay1ms(500); LaunchPad_Output(1); // red
        Clock_Delay1ms(500); LaunchPad_Output(0); // off
        Clock_Delay1ms(500); LaunchPad_Output(4); // blue
        }
}

//globals for program 3
#define PWMNOMINAL3 7500

#define SWING3 1000
#define PWMIN3 (PWMNOMINAL3-SWING3)
#define PWMAX3 (PWMNOMINAL3+SWING3)
#define DESIRED_SPEED 100


int32_t ActualSpeedL, ActualSpeedR;        // Actual speed
int32_t ErrorL, ErrorR;
int i =0;
int Kp6 = 3000; //Kp6 is divided by 100 later on in the progam, so this is really 30
 
//contorl for program 3
void Controller6(void){
	//checking if the robot hit a wall with bump sensors
	//if hit moves to mode 2
    if(Bump_Read()!= 0x3f){ // collision
           Mode = 2;
    
	}
	//updating time and tachometer everytime controller is called
    Time = Time + 1;
    Tachometer_Get(&LeftTach[i], &LeftDir, &LeftSteps, &RightTach[i], &RightDir, &RightSteps);
	//Mode 2, 90 degree turn
    if(Mode == 2){
		//first has robot back up a little to not hit wall again 
		if(tachStep == WAIT){
			Motor_Backward(PWMNOMINAL3, PWMNOMINAL3);
			 if(Time >= 300){
			  tachStep = 0;
			}
		}
		//otherwise turns the robot 90 degrees
		else{
			Motor_Stop();
			//getting the last tachometer reading 
			if(rightLast == -1){
				rightLast = RightSteps;
			}
			//calculating the amount of degrees the robot has moved
			tachStep = RightSteps - rightLast;
			turn =  (70 * tachStep) / 150;
			//if robot has turn around 90 or over 
			//85 is used because of the speed of the robot, 90 was too far 
			if( turn >= 85){
				//Mode back to normal straight mode
				Mode = 1;
				//reset globals
				Time = 0;
				rightLast = -1;
				tachStep = -1;

			}
			//if turn not complete keep turning 
			else{
			UR = PWMNOMINAL3;
			UL = PWMNOMINAL3;
			Motor_Right(UL, UR);
			Mode = 2;
			}
			//reseting
			ControllerFlag = 1;
			Time = 0;
		}
	}
    else{
		//mode 1, straight robot driving 
        if(Mode == 1){
            // pull tachometer information
            // use average of ten tachometer values to determine
            // actual speed (similar to Lab16)
            i = i + 1;
            if(i >= TACHBUFF){
                i = 0;
                ActualSpeedL = 2000000/avg(LeftTach, TACHBUFF);
			    ActualSpeedR = 2000000/avg(RightTach, TACHBUFF);
			    ErrorL = DESIRED_SPEED - ActualSpeedL;
			    ErrorR = DESIRED_SPEED - ActualSpeedR;

                // use proportional control to update duty cycle
                // LeftDuty = LeftDuty + Kp*LeftError
                UR = UR + ((Kp6*ErrorR)/100);
                UL = UL + ((Kp6*ErrorL)/100);

                // check min/max duty values
                if(PWMIN3 > UR){
                    UR = PWMIN3;
                }
                if(PWMAX3 < UR){
                    UR = PWMAX3;
                }
                if(PWMIN3 > UL){
                    UL = PWMIN3;
                }
                if(PWMAX3 < UL){
                    UL = PWMAX3;
                }

                // update motor values
                Motor_Forward(UL, UR);
                ControllerFlag = 1;
            }
        }
    }
}
//program 3 has a robot bump into a wall and turn 90 degrees left

void program3(void){
	//initialize functionality used by the robot
    DisableInterrupts();
    Clock_Init48MHz();
    LaunchPad_Init();
    Bump_Init();
    Reflectance_Init();
    Motor_Init();
    Tachometer_Init();
    TimerA1_Init(&Controller6,500);
    Motor_Stop();
    EnableInterrupts();
	
	//reseting Globals
    UR = UL = PWMNOMINAL3;    // reset parameters
    Mode = 1;
    ControllerFlag = 0;
    Time = 0;
    rightLast = -1;
    tachStep = WAIT;
    i =0;
    turn = 0;
    while(1){
		//resets controller flag
        if(ControllerFlag == 1){
            ControllerFlag = 0;
        }
    }
}

//globals for program 4
#define TOOCLOSE 110
#define DESIRED_DIST 172
#define TOOFAR 230


#define PWMNOMINAL4 6100
#define SWING4 500
#define PWMIN4 (PWMNOMINAL4-SWING4)
#define PWMAX4 (PWMNOMINAL4+SWING4)
scenario_t wall_state;

//used for readability 
#define  LEFTSTATE       0
#define  STRAIGHTSTATE   1
#define  TRANSITIONSTATE 2
#define  BLOCKEDSTATE    4
#define  RIGHTSTATE      6
#define  LEFTTURN        7

int waitLenght = 6;
int lastState = TRANSITIONSTATE;
int Kp2 = 8;
int turnDelay = 100;
volatile uint32_t nr, nc, nl; // raw distance values
int32_t Left, Center, Right; // IR distances in mm

volatile uint32_t ADCflag; // Set every 500us on ADC sample
int32_t DataBuffer[5];
int32_t SetPoint = 172;
uint32_t PosError;
int32_t Error2;

//Pause3 used to allow robot to get ready to start, waits 3 seconds
void Pause3(void){
    int j;
  while(Bump_Read() != 0x3f){ // wait for release
    Clock_Delay1ms(200); LaunchPad_Output(0); // off
    Clock_Delay1ms(200); LaunchPad_Output(1); // red
  }
  while(Bump_Read()== 0x3f){// wait for touch
    Clock_Delay1ms(100); LaunchPad_Output(0); // off
    Clock_Delay1ms(100); LaunchPad_Output(3); // red/green
  }
  while(Bump_Read() != 0x3f){ // wait for release
    Clock_Delay1ms(100); LaunchPad_Output(0); // off
    Clock_Delay1ms(100); LaunchPad_Output(4); // blue
  }
  for(j=1000;j>100;j=j-200){
    Clock_Delay1ms(j); LaunchPad_Output(0); // off
    Clock_Delay1ms(j); LaunchPad_Output(2); // green
  }
  // resets globals  
  UR = UL = PWMNOMINAL4;    // reset parameters
  rightLast = -1;
  tachStep = WAIT;
  j = 0;
  lastState = TRANSITIONSTATE;
  turn = 0;
  Mode = 1;
  ControllerFlag = 0;
  Time = 0;
}

//Clears LCD display 
void LCDClear2(void){
  Nokia5110_Init();
  Nokia5110_Clear(); // erase entire display
  Nokia5110_OutString("17: control");
  Nokia5110_SetCursor(0,1); Nokia5110_OutString("IR distance");
  Nokia5110_SetCursor(0,2); Nokia5110_OutString("L= "); Nokia5110_OutSDec(0); Nokia5110_OutString(" mm");
  Nokia5110_SetCursor(0,3); Nokia5110_OutString("C= "); Nokia5110_OutSDec(0); Nokia5110_OutString(" mm");
  Nokia5110_SetCursor(0,4); Nokia5110_OutString("R= "); Nokia5110_OutSDec(0); Nokia5110_OutString(" mm");
  Nokia5110_SetCursor(0,5); Nokia5110_OutString("E= "); Nokia5110_OutSDec(0); Nokia5110_OutString(" mm");
}
//outputs to LCD screen 
void LCDOut2(void){
  Nokia5110_SetCursor(3,2); Nokia5110_OutSDec(Left);
  Nokia5110_SetCursor(3,3); Nokia5110_OutSDec(Center);
  Nokia5110_SetCursor(3,4); Nokia5110_OutSDec(Right);
  Nokia5110_SetCursor(3,5); Nokia5110_OutSDec(Error);
  // left
  if(Time%5 == 0){
      UART0_OutUDec5(Left);UART0_OutString(" mm,");
      UART0_OutUDec5(Center);UART0_OutString(" mm,");
      UART0_OutUDec5(Right);UART0_OutString(" mm,");
      UART0_OutUDec5(UR);UART0_OutString(" %,");
      UART0_OutUDec5(UL);UART0_OutString(" %,");
      if(Error < 0){
          PosError = Error*(-1);
          UART0_OutString("-");UART0_OutUDec5(PosError);UART0_OutString("\n");
      }
      else{
          UART0_OutUDec5(Error);UART0_OutString("\n");
      }

  }
}

//Gettings information form the IR sensors
void IRsampling(void){
    uint32_t raw17, raw12, raw16;
    ADC_In17_12_16(&raw17, &raw12, &raw16);
    nr = LPF_Calc(raw17);
    nc = LPF_Calc2(raw12);
    nl = LPF_Calc3(raw16);
    Left = LeftConvert(nl);
    Center = CenterConvert(nc);
    Right = RightConvert(nr);
    ADCflag = 1;
}

/*
straightFunct - moves the robot between two walls using IR sensors
*/
void straightFunct(void){
	//waitLenght allows the robot to go straight for a little so it does
	//not get stuck in a turn
    if(Time <= waitLenght){
		if(Left > DESIRED_DIST && Right > DESIRED_DIST){
		  SetPoint = (Left + Right) / 2;
		}
		else{
		   SetPoint = 172;
		}
		// set error based off set point
		if(Left < Right){
			Error2 = Left - SetPoint;
		}
		 else{
		   Error2 = SetPoint - Right;
		}

		// update duty cycle based on proportional control
		UR = UR + Kp2 * Error2;
		UL = UL - Kp2 * Error2;
		// check to ensure not too big of a swing
		if(UR < PWMIN4){
		   UR = PWMIN4;
		}
		if(UR > PWMAX4){
		   UR = PWMAX4;
		}
		if(UL < PWMIN4){
		   UL = PWMIN4;
		}
		if(UL > PWMAX4){
		   UL = PWMAX4;
		}
		// update motor values
		Motor_Forward(UL, UR);
		//keeping the robot in straight functionality 
		lastState = TRANSITIONSTATE;
		}
    else{
		//resting globals 
		Time = 0;
		rightLast = -1;
		tachStep = WAIT;
		//will either move robot back into straight state or until another movement
		lastState = STRAIGHTSTATE;
    }
    ControllerFlag = 1;
}

/*
* Proportional controller to keep robot in
* center of two walls using IR sensors.
*/
void SysTick_Handler(void){
	//updating tachometer and Time each time the handler runs
    Tachometer_Get(&LeftTach[j], &LeftDir, &LeftSteps, &RightTach[j], &RightDir, &RightSteps);
    Time = Time + 1;
	//if the handler is in Mode exicutes movement code
    if(Mode){
		//classifies the state the robot is in 
        wall_state = Classify(Left, Center, Right);
		
		//robot will be in transition state if the robot just got out of any movement besides moving striaght 
		//transition moves robot into moving straight
        if(lastState == TRANSITIONSTATE){
           lastState = STRAIGHTSTATE;
           wall_state = Straight;
        }
		//if the wall state is a left joint, tee joint or cross road and the last state was either the Left state or straight state 
		//the robot will start or continue exicuting the left turn
        else if((wall_state == LeftJoint  || wall_state == TeeJoint || wall_state == CrossRoad ) && ( lastState == STRAIGHTSTATE || lastState == LEFTSTATE)){
            lastState = LEFTSTATE;
        }
		//if the wall state is leftTurn and the last state was either straight or left turn 
		//the robot will start or continue to exicute the left turn 
        else if ((wall_state == LeftTurn) && (lastState == STRAIGHTSTATE || lastState == LEFTTURN)){
            lastState = LEFTTURN;
        }
		//if robot sees a right movement and the last state was straight or right state
		//the robot will either start then right tunr functionality or continue it 
		else if((wall_state == RightTurn || wall_state == RightJoint) && (lastState == STRAIGHTSTATE || lastState == RIGHTSTATE)){
			lastState = RIGHTSTATE;
		}
		//if the robot is blocked and the last state was blocked or straight state
		//robot will exicutre 180 turn or continue its 180 turn 
        else if(wall_state == Blocked && ( lastState == BLOCKEDSTATE || lastState == STRAIGHTSTATE)){
            lastState = BLOCKEDSTATE;
        }
		//making sure the wall_state is correct based on the last state
        if(lastState == BLOCKEDSTATE){
            wall_state = Blocked;
        }
        if(lastState == LEFTSTATE){
            wall_state = LeftJoint;
        }
		if(lastState == RIGHTSTATE){
			wall_state = RightTurn;
		}
        if(lastState == STRAIGHTSTATE){
             wall_state = Straight;
        }
        if(lastState == LEFTTURN){
            wall_state = LeftTurn;
        }
		//if robot should move straight, exicutes straight functionality
        if(wall_state == Straight && lastState == STRAIGHTSTATE){
            straightFunct();
        }
		//if robot should turn left, exicutes left turn
        else if((wall_state == LeftJoint || wall_state == TeeJoint || wall_state == CrossRoad) && lastState == LEFTSTATE){
			//setting the waitLenght for after the turn is finished, makes for cleaner movement 
            waitLenght = 6;
			//moves robot forward so left turn doesnt put robot into all
            if(tachStep == WAIT){
                Motor_Forward(PWMNOMINAL4, PWMNOMINAL4);
				if(Time > turnDelay){
					tachStep = 0;
				}
            }
			//if robot has already moved forward a little left turn is exicuted
            else{
				//getting the last tachoment reading 
                if(rightLast == -1){
                   rightLast = RightSteps;
                }
				//checking how many degrees the robot has moved
                tachStep = RightSteps - rightLast;
                turn =  (70 * tachStep) / 150;
				//if has exicuted around 90 degree turn exit and move back to straight mode
                if( turn >= 87){
                    lastState = TRANSITIONSTATE;
					//reseting globals
                    rightLast = -1;
                    tachStep = WAIT;
                    Time = 0;
                    turn = 0;
                }
				//if turn is not done continue turning 
				else{
					Motor_Right(PWMNOMINAL4, PWMNOMINAL4);
                }
				//resting time
                 Time = 0;
			}
			//resting motor value 
            ControllerFlag = 1;
        }
		//if robot sees a left turn also turns left 
        else if( wall_state == LeftTurn && lastState == LEFTTURN){
			//setting the waitLenght for after the turn is finished, makes for cleaner movement 
            waitLenght = 0;
			//allowing robot to move forward a little so it does not hit a wall
            if(tachStep == WAIT){
                Motor_Forward(PWMNOMINAL4, PWMNOMINAL4);
                if(Time > turnDelay + 5){
                    tachStep = 0;
                }
            }
			//after robot has moved forward a little 
            else{
				//getting last tachometer reading 
				if(rightLast == -1){
				   rightLast = RightSteps;
				}
				//getting how many degrees the robot has turned
				tachStep = RightSteps - rightLast;
				turn =  (70 * tachStep) / 150;
				//if robot has turned around 90 degrees exit turn state and move to straight state
				if( turn >= 87){
					lastState = TRANSITIONSTATE;
					//reseting globals
					rightLast = -1;
					tachStep = WAIT;
					Time = 0;
					turn = 0;
				}
				//if turn not complete keep exicuting 
				else{
				  Motor_Right(PWMNOMINAL4, PWMNOMINAL4);
				}
				//reseting globals 
				Time = 0;
				ControllerFlag = 1;
            }
        }
		//if robot sees right movement, move robot forward so it doesn't hit a wall 
		else if((wall_state == RightTurn || wall_state == RightJoint) && lastState == RIGHTSTATE){
			//move robot forward 
		    Motor_Forward(PWMNOMINAL4, PWMNOMINAL4);
			//once done move to straight state
			if(Time > turnDelay){
				lastState = TRANSITIONSTATE;
				//reset globals 
				Time = 0;
				tachStep = WAIT;
				turn = 0;
			}
		}
		//if robot is blocked exicute 180 degree turn 
        else if(wall_state == Blocked && lastState == BLOCKEDSTATE){
            tachStep = 0;
            Motor_Stop();
			//getting last tachometer reading 
            if(rightLast == -1){
                rightLast = RightSteps;
            }
			//getting the degrees the robot has turned 
            tachStep = RightSteps - rightLast;
            turn =  (70 * tachStep) / 150;
			//if robot has turned around 180 degrees move back to driving straight 
            if( turn >= 175){
                lastState = TRANSITIONSTATE;
				//reset globals 
                Time = 0;
                rightLast = -1;
                tachStep = WAIT;
                turn = 0;
             }
			 //if turn not complete keep turning 
			 else{
               Motor_Right(PWMNOMINAL4, PWMNOMINAL4);
             }
              ControllerFlag = 1;
              Time = 0;
        }
		//if somehow robot gets here move robot straight 
        else{
            ControllerFlag = 1;
            Motor_Stop();
            lastState = STRAIGHTSTATE;
        }
    }
}

//program 4 moves robot through maze using IR sensors 

void program4(void){
	//initializing funtionality used by robot
    uint32_t raw17,raw12,raw16;
    DisableInterrupts();
    Clock_Init48MHz();
    LaunchPad_Init();
    Bump_Init();
    Motor_Init();
    Tachometer_Init();
    // user TimerA1 to sample the IR sensors at 2000 Hz
    // replace this line with a call to TimerA1_Init()
    TimerA1_Init(&IRsampling, 250);
    Motor_Stop();
    LCDClear2();
	
	//setting globals 
    Mode = 0;
    ADCflag = ControllerFlag = 0;   // semaphores
    rightLast = -1;
    tachStep = WAIT;
    j = 0;
    lastState = STRAIGHTSTATE;
    turn = 0;
    ADC0_InitSWTriggerCh17_12_16();   // initialize channels 17,12,16
    ADC_In17_12_16(&raw17,&raw12,&raw16);  // sample
    LPF_Init(raw17,64);     // P9.0/channel 17
    LPF_Init2(raw12,64);    // P4.1/channel 12
    LPF_Init3(raw16,64);    // P9.1/channel 16

    // user SysTick to run the controller at 100 Hz with a priority of 2
    // replace this line with a call to SysTick_Init()
    SysTick_Init(480000,2);
    Pause3();

    EnableInterrupts();
    Motor_Forward(UL,UR);
    while(1){

        if(ControllerFlag == 1){ // 100 Hz, not real time
            LCDOut2();
            ControllerFlag = 0;
        }
    }
}
 int main(void){
  //  program1();
  //  program2();
   // program3();
   program4();
}
